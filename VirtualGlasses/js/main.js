let dataGlass = [
    { id: "G1", src: "./img/g1.jpg", virtualImg: "./img/v1.png", brand: "Armani Exchange", name: "Bamboo wood", color: "Brown", price: 150, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis ea voluptates officiis? " },
    { id: "G2", src: "./img/g2.jpg", virtualImg: "./img/v2.png", brand: "Arnette", name: "American flag", color: "American flag", price: 150, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. In assumenda earum eaque doloremque, tempore distinctio." },
    { id: "G3", src: "./img/g3.jpg", virtualImg: "./img/v3.png", brand: "Burberry", name: "Belt of Hippolyte", color: "Blue", price: 100, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit." },
    { id: "G4", src: "./img/g4.jpg", virtualImg: "./img/v4.png", brand: "Coarch", name: "Cretan Bull", color: "Red", price: 100, description: "In assumenda earum eaque doloremque, tempore distinctio." },
    { id: "G5", src: "./img/g5.jpg", virtualImg: "./img/v5.png", brand: "D&G", name: "JOYRIDE", color: "Gold", price: 180, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Error odio minima sit labore optio officia?" },
    { id: "G6", src: "./img/g6.jpg", virtualImg: "./img/v6.png", brand: "Polo", name: "NATTY ICE", color: "Blue, White", price: 120, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit." },
    { id: "G7", src: "./img/g7.jpg", virtualImg: "./img/v7.png", brand: "Ralph", name: "TORTOISE", color: "Black, Yellow", price: 120, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim sint nobis incidunt non voluptate quibusdam." },
    { id: "G8", src: "./img/g8.jpg", virtualImg: "./img/v8.png", brand: "Polo", name: "NATTY ICE", color: "Red, Black", price: 120, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit, unde enim." },
    { id: "G9", src: "./img/g9.jpg", virtualImg: "./img/v9.png", brand: "Coarch", name: "MIDNIGHT VIXEN REMIX", color: "Blue, Black", price: 120, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit consequatur soluta ad aut laborum amet." }
];


let glassSample = () => {
    let glassItem = dataGlass.map(item =>{
        return `<img onclick="checkGlass('${item.virtualImg}')" class="mx-auto mt-5 py-3" style="width:30%; object-fit:contain; cursor:pointer" src="${item.virtualImg}" alt="">`;
    });
    let glassItem2 = glassItem.join('');
    document.querySelector('#vglassesList').innerHTML = `${glassItem2}`;
};
window.onload = glassSample

let checkGlass = (item) =>{
    document.querySelector('.vglasses__model').innerHTML = `<img class="photoContent" src="${item}" alt="">`
    let index = dataGlass.findIndex(data => {
        return data.virtualImg === item
    });
    if(index || index == 0){
        document.querySelector('.vglasses__info').style.display = `block`;
        document.querySelector('.vglasses__info').innerHTML =
        `
            <h2 style="margin-bottom:20px; font-size:20px; font-weight:600">${dataGlass[index].name} - ${dataGlass[index].brand}</h2>
            <div style="display:flex">
                <p style="background-color:orange; width:40px; heigh:40px; text-align:center">${dataGlass[index].price}<p>
                <p style="color:aqua; margin-left:20px">Stocking</p> 
            </div>
            <p>${dataGlass[index].description}</p>
        `
    };
};
window.checkGlass = checkGlass

let removeGlasses = (status)=>{
    if(!status){
        document.querySelector('.photoContent').style.display = 'none'
    } else{
        document.querySelector('.photoContent').style.display = 'block'
    }
}
window.removeGlasses = removeGlasses






